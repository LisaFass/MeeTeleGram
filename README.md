# MeeTeleGram
An Unofficial Telegram Client for MeeGo OS (Nokia N9), based on tgl/ library.

# Contribution
If you want to contribute, please fork the project and create merge requests
against the project. Thank you for your participation!
